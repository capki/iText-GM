package cn.jiedanba.itext5.gm.sign.test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.Certificate;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1GeneralizedTime;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERUTF8String;
import org.bouncycastle.asn1.gm.GMObjectIdentifiers;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Test;
import org.ofdrw.gm.ses.v4.SES_CertList;
import org.ofdrw.gm.ses.v4.SES_ESPictrueInfo;
import org.ofdrw.gm.ses.v4.SES_ESPropertyInfo;
import org.ofdrw.gm.ses.v4.SES_Header;
import org.ofdrw.gm.ses.v4.SES_SealInfo;
import org.ofdrw.gm.ses.v4.SESeal;

import cn.jiedanba.itext5.util.PkiUtil;

/**
 * 国密印章制作
 * 
 * @author dell
 *
 */
public class MakeESSealTest {
	private static String cert = "MIIDFTCCArygAwIBAgIPBslAv+CvdtvpAz1PUBxgMAoGCCqBHM9VAYN1MIGIMQswCQYDVQQGEwJDTjESMBAGA1UECAwJR3VhbmdEb25nMREwDwYDVQQHDAhTaGVuWmhlbjE5MDcGA1UECgwwRGlnaXRhbCBDZXJ0aWZpY2F0ZSBBdXRob3JpemF0aW9uIENlbnRlciBDby4gTHRkMRcwFQYDVQQDDA5HbG9iYWwgU3ViIENBMjAeFw0yMzA3MDcwOTEwNTdaFw0yNDA3MDYwOTEwNTdaMGExCzAJBgNVBAYTAkNOMRIwEAYDVQQIDAnlub/kuJznnIExEjAQBgNVBAcMCea3seWcs+W4gjEqMCgGA1UEAwwh5rex5Zyz5biC5rWL6K+V56eR5oqA5pyJ6ZmQ5YWs5Y+4MFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAEm3bkCoCaQJxm+zo1hvQnnMt8MLy5+CxukQZ1NBZOTS3cfX5la2fYyGXgRQgj1A2O/m6wN4Pz1Y+nQFSYVnRPEaOCAS0wggEpMA4GA1UdDwEB/wQEAwIGwDAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFHfjfwKCBeuIUN8/kOEWZ0LG3g+EMB0GA1UdDgQWBBRij3/LGGPBwWaUztDMj13CYrv7ODAgBgNVHSUBAf8EFjAUBggrBgEFBQcDAgYIKwYBBQUHAwQwOgYDVR0fBDMwMTAvoC2gK4YpaHR0cDovLzEzOS45Ljg5LjExMjo4MDg1L0dsb2JhbFN1YkNBMi5jcmwwawYIKwYBBQUHAQEEXzBdMCQGCCsGAQUFBzABhhhodHRwOi8vMTM5LjkuODkuMTEyOjgwODIwNQYIKwYBBQUHMAKGKWh0dHA6Ly8xMzkuOS44OS4xMTI6ODA4NS9HbG9iYWxTdWJDQTIuY3J0MAoGCCqBHM9VAYN1A0cAMEQCIGc9hjfu3AAG9O2amxgegmfy2vmw40SyhrpPvcUFrhoGAiBJP5HSOpXsUfMpVKdNopNgLjQZOUry8ondN9jKVhY6rg==";
	private static String privateKey = "MIGTAgEAMBMGByqGSM49AgEGCCqBHM9VAYItBHkwdwIBAQQgBGo3dacOLVg9J1uGZxZ77QFduzZL+R//vve5v79vSS6gCgYIKoEcz1UBgi2hRANCAASbduQKgJpAnGb7OjWG9Cecy3wwvLn4LG6RBnU0Fk5NLdx9fmVrZ9jIZeBFCCPUDY7+brA3g/PVj6dAVJhWdE8R";

	@Test
	public void make() throws Exception {
		Certificate sealerCert = PkiUtil.readX509Certificate(Base64.decodeBase64(cert));
		Certificate userCert = PkiUtil.readX509Certificate(Base64.decodeBase64(cert));

		ASN1EncodableVector v = new ASN1EncodableVector(1);
		v.add(new DEROctetString(userCert.getEncoded()));

		SES_Header header = new SES_Header(SES_Header.V4, new DERIA5String("GMSEAL"));
		Calendar now = Calendar.getInstance();
		now.add(Calendar.YEAR, 2);
		Date then = now.getTime();
		SES_ESPropertyInfo propertyInfo = new SES_ESPropertyInfo().setType(new ASN1Integer(3))
				.setName(new DERUTF8String("深圳市测试科技有限公司公章")).setCertListType(SES_ESPropertyInfo.CertListType)
				.setCertList(SES_CertList.getInstance(SES_ESPropertyInfo.CertListType, new DERSequence(v)))
				.setCreateDate(new ASN1GeneralizedTime(new Date())).setValidStart(new ASN1GeneralizedTime(new Date()))
				.setValidEnd(new ASN1GeneralizedTime(then));
		Path sealImage = Paths.get("src/main/resources", "深圳测试科技有限公司_公章.png");

		SES_ESPictrueInfo pictrueInfo = new SES_ESPictrueInfo().setType("PNG").setData(Files.readAllBytes(sealImage))
				.setWidth(40).setHeight(40);

		SES_SealInfo sesSealInfo = new SES_SealInfo().setHeader(header)
				.setEsID(new DERIA5String(UUID.randomUUID().toString().replace("-", ""))).setProperty(propertyInfo)
				.setPicture(pictrueInfo);

		PrivateKey sealerPrvKey = PkiUtil.getPrivateKey(Base64.decodeBase64(privateKey));
		Signature sg = Signature.getInstance("SM3WithSM2", new BouncyCastleProvider());
		sg.initSign(sealerPrvKey);
		sg.update(sesSealInfo.getEncoded("DER"));
		byte[] sigVal = sg.sign();

		SESeal seal = new SESeal().seteSealInfo(sesSealInfo).setCert(sealerCert)
				.setSignAlgID(GMObjectIdentifiers.sm2sign_with_sm3).setSignedValue(sigVal);

		Path out = Paths.get("D://UserV4.esl");
		Files.write(out, seal.getEncoded("DER"));
		System.out.println(">> V4版本印章存储于: " + out.toAbsolutePath().toAbsolutePath());
	}
}
