package cn.jiedanba.itext5.signhash.test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.security.TSAClient;
import com.itextpdf.text.pdf.security.TSAClientBouncyCastle;

import cn.jiedanba.itext5.check.CRLCheck;
import cn.jiedanba.itext5.check.OCSPCheck;
import cn.jiedanba.itext5.gm.sign.ITextGM;
import cn.jiedanba.itext5.gm.sign.vo.GetPdfHash;
import cn.jiedanba.itext5.gm.sign.vo.GetPdfHashParamVo;
import cn.jiedanba.itext5.rsa.signHash.ITextSignHashUtil;
import cn.jiedanba.itext5.util.PkiUtil;

/**
 * pdf分离式签名 . 场景：从外部设备获取p1数据，例如ukey，签验签服务器
 * 
 * @author dell
 *
 */
public class SignHashTest {

	private static String pkStr = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDTDAo0j3tVnJ4oUMef2XGiZg/fS/nNoriec/m7Jms7kBdkFwDyspuXY89j+Q8/GlazMIn87f+Sof/fzSJQAcU//GnR61H+2keJ6w49FvxQQCu1DmjpYvHmB6019kIVJ93oAEMc7OF5SVNw4Pw0uu052hv0Pe1L3srcCsYOgq8nNH8xVQsy801FoFJq6ejqx00zWGnA8Mdkoh+DhqYvW80RLv6qmfHdfYBYi3CfBBXucVM9pjv4l2ljxBxMygW3zj7kslx5G/3ZUhACzOiaLdy+aZNlNUaQxqu7lZNLexixZrqQ2+Gr/s2sIiOZX2I/bqSgMLE4Lo7nrdaJuvWcke8FAgMBAAECggEABNOGX6Yw6kfbnXr6SkP0A3IC6zIyTqsgrESuaFUXldQd4EA6a8AhmMFoSE0jER8S7n+JPN2lCCPWQyQXpKR1uQZx+jSJA6NpY6/jsnfxqzAlogQxDtr7sFGzw+gBD7lc9MEd7FuO/00y+W+3IoBsZ4YjgczMvf0ALJxQv3bF5KKV5Ze6+Wz18kuzjwVW9qE4hcwfMVyZH2+8HI5YStBl+ACQzewS65zQTcIVesscBSRaUg2hcDHherBsRZtiR8gYesKkXSUlGfL+/sqbYyrctTyx+21Adb678Avp+zjyj/uBsVk9TBEgbRctrUG19vl+hCv7joJtbdJvRN0IWrW2oQKBgQD6TJKu77l4BI+q0daBs8xxU5d4GVETQ9DFxKopxJopMgr2s1+183fvPBMDUj3cY1MaQ682cGbSvMhW/+XukK6S2Nyw99D2UPf+IpMkPV/mYHHaczVSd5Dn17TPkV9YGbKIaZGIfWcDxdQAJk8padrD/7et7pcudyKclIdiw8hvTQKBgQDX2pk4OAMb+cxtcmqLgWxG8kMhLiNkFYtFvDJbkcEwbmLTZNyIim+uMrtelQZ1PHr34EcyBaJVG+8WoG22OYgdwlcAxvJcF+pw1d++DnsQ7HqZ3GN70nwShqntxcKf5/MoztIhz4Yo1ahRl5/Whis1CokdmzGhpcmhfiqa98QSmQKBgQCe4+sd2BoxwRt+gbSdI4k9y6XU3KVmCF/Im961oxOF4TmqgBXbCFJzcPxU2GlitT605pCci2AKmULIT6wj9H6Nw9LdGIPzSVUUY9hqYfe15DWCSi5oSnSXaIxkU9FluPJnvDRho9G4SGYbLjaEgKkaUQPGCza3jXNQWM1U+hoL5QKBgQCTBvMWuBBmHeCBLE4meCGosZvIHgU9Ru92sJochUtyEb5+/1S4LJOltCisZBXiBTA9ZycXi37nOf/BCcWxOb7dhmJ8xQaQdDncYdHlQzfQb0OTSer5jIW2mMNmki3ffb7DLR00qoZXijD8YqsdFIaZunNrFyrXUHXm6mla5SlJAQKBgQC8G9tM71g1paWzoniO41ggtOMesjU+uqaM5Q01jT3V0TbE5pkdf7zadA2iRTCv6DGdCRdz1eIi+pcLci8A1iJvt+r2aVOKtPU/FfC2h8WyFT2ggSnhBGZ6CfOocCNbYB8Dgnz/KBRFHgebvDpEVtxg0NqmPoH8UA1m7zcB6VlXEA==";

	private static String certStr = "MIIFEzCCA/ugAwIBAgIKJYchqs8+nl/gXTANBgkqhkiG9w0BAQsFADBoMQswCQYDVQQGEwJDTjE2MDQGA1UECgwtRGlnaXRhbCBDZXJ0aWZpY2F0ZSBBdXRob3JpdHkgQ2VudGVyIENvLiwgTHRkMSEwHwYDVQQDDBhHbG9iYWwgSWRlbnRpdHkgQ0EgLSBSU0EwHhcNMjMwOTE1MTQwNTU1WhcNMjUwOTE0MTQwNTU1WjB1MQswCQYDVQQGEwJDTjEPMA0GA1UECAwG5bm/5LicMQ8wDQYDVQQHDAbmt7HlnLMxGzAZBgNVBAoMEjkxNDQwMzAwMDg4MzQxNTEwMDEnMCUGA1UEAwwe5rex5Zyz5rWL6K+V56eR5oqA5pyJ6ZmQ5YWs5Y+4MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0wwKNI97VZyeKFDHn9lxomYP30v5zaK4nnP5uyZrO5AXZBcA8rKbl2PPY/kPPxpWszCJ/O3/kqH/380iUAHFP/xp0etR/tpHiesOPRb8UEArtQ5o6WLx5getNfZCFSfd6ABDHOzheUlTcOD8NLrtOdob9D3tS97K3ArGDoKvJzR/MVULMvNNRaBSauno6sdNM1hpwPDHZKIfg4amL1vNES7+qpnx3X2AWItwnwQV7nFTPaY7+JdpY8QcTMoFt84+5LJceRv92VIQAszomi3cvmmTZTVGkMaru5WTS3sYsWa6kNvhq/7NrCIjmV9iP26koDCxOC6O563Wibr1nJHvBQIDAQABo4IBsDCCAawwDgYDVR0PAQH/BAQDAgbAMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUyismFJosCD6o+VNa23bShDGWc/MwHQYDVR0OBBYEFNB2ddN5L6shYjnN4xlVssFOv54qMBIGCCsGAQQBCXADBAYWBFRlc3QwIAYDVR0lAQH/BBYwFAYIKwYBBQUHAwQGCCsGAQUFBwMCMEQGA1UdHwQ9MDswOaA3oDWGM2h0dHA6Ly9jcmwuamllZGFuYmEuY24vY3JsL0dsb2JhbElkZW50aXR5Q0EtUlNBLmNybDCBgwYIKwYBBQUHAQEEdzB1MDIGCCsGAQUFBzABhiZodHRwOi8vb2NzcC5qaWVkYW5iYS5jbi9vY3NwL29jc3BRdWVyeTA/BggrBgEFBQcwAoYzaHR0cDovL2NydC5qaWVkYW5iYS5jbi9jcnQvR2xvYmFsSWRlbnRpdHlDQS1SU0EuY3J0MEoGA1UdIARDMEEwNQYJYIZIAYb9bAEBMCgwJgYIKwYBBQUHAgEWGmh0dHA6Ly9jcHMuamllZGFuYmEuY24vY3BzMAgGBmeBDAECAjANBgkqhkiG9w0BAQsFAAOCAQEA270mPV1awAGM5iDnUOiIEQ3FN5MltQOc3suuMLq+HeGqLu48tMrLhpjzW/4P6ACy+qxBN4o9vaZkHn8gNIf5L4WrmzssnCzyK/ITMg5XfYpo9oNgHBqUGlPvXXZ+OSsuyJNK9nEHrcCNzN4eSohxNUwcKNVyAAB2OCIH2dqpfRG64IKOqN1Xkd+D7gO7LTOH7XDa4K/95YfVsqCzE3jEgkND15eAy/DegbIOM+u2xMxjIeOz3ye+GW3D4gMl0YWt7SyaSJ761KEkSpkqd59B+4WNXo4Oowd90zGJRnb6pke2CnKJaz5o119WGvVVMSsaqXbhZL3EJ8nE6/lSiFYRXQ==";

	public static void sign() throws IOException, GeneralSecurityException, DocumentException {
		GetPdfHashParamVo paramVo = new GetPdfHashParamVo();
		Path pdf = Paths.get("src/main/resources", "test.pdf");
		paramVo.setPdf(Files.readAllBytes(pdf));
		paramVo.setPageNo(1);
		paramVo.setLlx(240);
		paramVo.setLly(290);
		paramVo.setUrx(paramVo.getLlx() + 120);
		paramVo.setUry(paramVo.getLly() + 120);
		paramVo.setLocation("深圳");
		paramVo.setReason("分离式签名测试");
		paramVo.setHashAlgorithm("SHA256");
		Path sealImage = Paths.get("src/main/resources", "深圳测试科技有限公司_公章.png");
		paramVo.setSignImage(Files.readAllBytes(sealImage));

		// 创建签名域，获取pdf文件摘要
		GetPdfHash getPdfHash = ITextSignHashUtil.getPdfHash(paramVo);
		System.out.println(Base64.encodeBase64String(getPdfHash.getDigesHash()));
		TSAClient tsaClient = new TSAClientBouncyCastle("http://43.139.245.54:8082/tsa/sign?type=RSA", null, null, 4096, "SHA256");

		PrivateKey prvKey = PkiUtil.getPrivateKey(Base64.decodeBase64(pkStr));

		X509Certificate signCert = PkiUtil.readX509Certificate(Base64.decodeBase64(certStr));

		// 使用OCSP检查证书状态
		OCSPCheck ocspCheck = new OCSPCheck(signCert);
		boolean ocspCheckStatus = ocspCheck.checkCertStatus();
		System.out.println("OCSP查询证书状态：" + (ocspCheckStatus == true ? "有效" : "吊销"));
		// 使用CRL检查证书状态
		CRLCheck crlCheck = new CRLCheck(signCert);
		boolean crlCheckStatus = crlCheck.checkCertStatus();
		System.out.println("CRL吊销列表检查证书状态：" + (crlCheckStatus == true ? "有效" : "吊销"));
		// 签署p1，以下为模拟外部签名测试
		byte[] p1 = PkiUtil.sign(prvKey, "SHA256WithRSA", getPdfHash.getSignHash());

		// 签署p7
		byte[] p7 = ITextSignHashUtil.signHash(getPdfHash.getDigesHash(), p1,
				PkiUtil.getCertificateChain(signCert.getEncoded()), "SHA256", tsaClient);

		// 签署pdf
		byte[] signSuccess = ITextGM.signDeferred(getPdfHash.getEmptySignaturePdf(), p7, getPdfHash.getFieldName());
		File file = new File("src/main/resources/sign_hash.pdf");
		FileUtils.writeByteArrayToFile(file, signSuccess);
		System.out.println("签名完成：" + file.getPath());
	}

	public static void main(String[] args) throws IOException, GeneralSecurityException, DocumentException {
		sign();
	}
}
