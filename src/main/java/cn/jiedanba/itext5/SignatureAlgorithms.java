package cn.jiedanba.itext5;

import java.util.HashMap;

/**
 * 
 * 签名算法
 *
 */
public class SignatureAlgorithms {

	private static final HashMap<String, String> signatureAlgOID = new HashMap<String, String>();

	private static final HashMap<String, String> signatureAlg = new HashMap<String, String>();

	static {
		signatureAlgOID.put("1.2.840.113549.1.1.4", "MD5WithRSA");

		/* SHA PKCS #1 v1.5 */
		signatureAlgOID.put("1.2.840.113549.1.1.5", "SHA1WithRSA");
		signatureAlgOID.put("1.2.840.113549.1.1.11", "SHA256WithRSA");
		signatureAlgOID.put("1.2.840.113549.1.1.12", "SHA384WithRSA");
		signatureAlgOID.put("1.2.840.113549.1.1.13", "SHA512WithRSA");
		/* SHA PKCS #1 v1.5 */

		/* 国密 PKCS #1 v1.5 */
		signatureAlgOID.put("1.2.156.10197.1.501", "SM3WithSM2");
		signatureAlgOID.put("1.2.156.10197.1.502", "SHA1WithSM2");
		signatureAlgOID.put("1.2.156.10197.1.503", "SHA256WithSM2");
		signatureAlgOID.put("1.2.156.10197.1.504", "SM3WithRSA");
		/* 国密 PKCS #1 v1.5 */

		/* ECDSA PKCS #1 v1.5 */
		signatureAlgOID.put("1.2.840.10045.4.1", "SHA1WithECDSA");
		signatureAlgOID.put("1.2.840.10045.4.3.2", "SHA256WithECDSA");
		signatureAlgOID.put("1.2.840.10045.4.3.3", "SHA384WithECDSA");
		signatureAlgOID.put("1.2.840.10045.4.3.4", "SHA512WithECDSA");
		/* ECDSA PKCS #1 v1.5 */

		/* SHA3 RSA PKCS #1 v1.5 */
		signatureAlgOID.put("2.16.840.1.101.3.4.3.10", "SHA3-256WithRSA");
		signatureAlgOID.put("2.16.840.1.101.3.4.3.11", "SHA3-384WithRSA");
		signatureAlgOID.put("2.16.840.1.101.3.4.3.12", "SHA3-512WithRSA");
		/* SHA3 RSA PKCS #1 v1.5 */

		/* SHA3 ECDSA PKCS #1 v1.5 */
		signatureAlgOID.put("2.16.840.1.101.3.4.3.10", "SHA3-256WithECDSA");
		signatureAlgOID.put("2.16.840.1.101.3.4.3.11", "SHA3-384WithECDSA");
		signatureAlgOID.put("2.16.840.1.101.3.4.3.12", "SHA3-512WithECDSA");
		/* SHA3 ECDSA PKCS #1 v1.5 */

		signatureAlg.put("SHA1WITHRSA", "1.2.840.113549.1.1.5");
		signatureAlg.put("SHA256WITHRSA", "1.2.840.113549.1.1.11");
		signatureAlg.put("MD5WITHRSA", "1.2.840.113549.1.1.4");
		signatureAlg.put("SHA384WITHRSA", "1.2.840.113549.1.1.12");
		signatureAlg.put("SHA512WITHRSA", "1.2.840.113549.1.1.13");
		signatureAlg.put("SM3WITHSM2", "1.2.156.10197.1.501");
		signatureAlg.put("SHA256WITHSM2", "1.2.156.10197.1.503");
		signatureAlg.put("SHA1WITHSM2", "1.2.156.10197.1.502");
		signatureAlg.put("SM3WITHRSA", "1.2.156.10197.1.504");
		signatureAlg.put("SHA1WITHECDSA", "1.2.840.10045.4.1");
		signatureAlg.put("SHA256WITHECDSA", "1.2.840.10045.4.3.2");
		signatureAlg.put("SHA384WITHECDSA", "1.2.840.10045.4.3.3");
		signatureAlg.put("SHA512WITHECDSA", "1.2.840.10045.4.3.4");
		signatureAlg.put("SHA3-256WithRSA", "2.16.840.1.101.3.4.3.10");
		signatureAlg.put("SHA3-384WithRSA", "2.16.840.1.101.3.4.3.11");
		signatureAlg.put("SHA3-512WithRSA", "2.16.840.1.101.3.4.3.12");
		signatureAlg.put("SHA3-256WithECDSA", "2.16.840.1.101.3.4.3.10");
		signatureAlg.put("SHA3-384WithECDSA", "2.16.840.1.101.3.4.3.11");
		signatureAlg.put("SHA3-512WithECDSA", "2.16.840.1.101.3.4.3.12");

	}

	/**
	 * 获取签名算法名称
	 * 
	 * @param oid
	 * @return
	 */
	public static String getSignatureName(String oid) {
		if (oid == null) {
			throw new IllegalArgumentException("Oid cannot be empty");
		}
		String signName = signatureAlgOID.get(oid);
		if (signName == null) {
			throw new RuntimeException("Oid does not exist");
		}
		return signName;
	}

	/**
	 * 获取签名算法 oid
	 * 
	 * @param signName
	 *            算法名称
	 * @return
	 */
	public static String getSignatureOid(String signName) {
		if (signName == null) {
			throw new IllegalArgumentException("signName cannot be empty");
		}
		String oid = signatureAlg.get(signName.toUpperCase());
		if (oid == null) {
			throw new RuntimeException("signName does not exist");
		}
		return oid;
	}

}
