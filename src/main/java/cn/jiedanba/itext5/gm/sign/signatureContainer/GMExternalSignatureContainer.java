package cn.jiedanba.itext5.gm.sign.signatureContainer;

import java.io.InputStream;
import java.security.GeneralSecurityException;

import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.security.ExternalSignatureContainer;

public class GMExternalSignatureContainer implements ExternalSignatureContainer {
	private PdfDictionary sigDic;

	public GMExternalSignatureContainer(PdfDictionary sigDic) {
		this.sigDic = sigDic;
	}

	public GMExternalSignatureContainer() {
		sigDic = new PdfDictionary();
		sigDic.put(PdfName.FILTER, new PdfName("GM.PKILite"));
		sigDic.put(PdfName.SUBFILTER, new PdfName("GM.sm2seal"));

	}

	@Override
	public byte[] sign(InputStream data) throws GeneralSecurityException {
		return new byte[0];
	}

	@Override
	public void modifySigningDictionary(PdfDictionary signDic) {
		signDic.putAll(sigDic);
	}
}
